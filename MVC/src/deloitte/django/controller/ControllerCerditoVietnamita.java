package deloitte.django.controller;

import java.util.ArrayList;

import deloitte.django.model.CerditoVietnamita;

public class ControllerCerditoVietnamita {
	/**
	 * Este m�todo permite agregar un cerdito vietnamita a una lista indicada
	 * siempre y cuando se den todos los atributos del cerdito a crear.
	 * 
	 * @param edad lorem ipsum dolor sit amet
	 * @param pesoEnGramos lorem ipsum dolor sit amet
	 * @param sabeDespejar lorem ipsum dolor sit amet
	 * @param sabeDerivar lorem ipsum dolor sit amet
	 * @param nombre lorem ipsum dolor sit amet
	 * @param pelaje lorem ipsum dolor sit amet
	 * @param atributo7 lorem ipsum dolor sit amet
	 * @param atributo8 lorem ipsum dolor sit amet
	 * @param atributo9 lorem ipsum dolor sit amet
	 * @param atributo10 lorem ipsum dolor sit amet
	 * @param atributo11 lorem ipsum dolor sit amet
	 * @param atributo12 lorem ipsum dolor sit amet
	 * @param atributo13 lorem ipsum dolor sit amet
	 * @param atributo14 lorem ipsum dolor sit amet
	 * @param atributo15 lorem ipsum dolor sit amet
	 * @param atributo16 lorem ipsum dolor sit amet
	 * @param atributo17 lorem ipsum dolor sit amet
	 * @param atributo18 lorem ipsum dolor sit amet
	 * @param atributo19 lorem ipsum dolor sit amet
	 * @param atributo20 lorem ipsum dolor sit amet
	 * @param listaDeCerditos lorem ipsum dolor sit amet
	 */
	public static void agregarCerditoVietnamita(int edad, double pesoEnGramos,
			Boolean sabeDespejar, Boolean sabeDerivar,
			String nombre, String pelaje,
			String atributo7, String atributo8,
			String atributo9, String atributo10,
			String atributo11, String atributo12,
			String atributo13, String atributo14,
			String atributo15, String atributo16,
			String atributo17, String atributo18,
			String atributo19, String atributo20, ArrayList<CerditoVietnamita> listaDeCerditos) {
		
		CerditoVietnamita cv = new CerditoVietnamita(edad, pesoEnGramos,
				sabeDespejar, sabeDerivar,
				nombre, pelaje,
				atributo7, atributo8,
				atributo9, atributo10,
				atributo11, atributo12,
				atributo13, atributo14,
				atributo15, atributo16,
				atributo17, atributo18,
				atributo19, atributo20);
		
		listaDeCerditos.add(cv);
	}
	
	/**
	 * Este m�tod permite agregar un cerdito vietnamita a una lista dada
	 * @param cv el objeto de tipo CerditoVietnamita para agregar a la lista
	 * @param listaDeCerditos la lista en la que ser� almacenado el cerdito
	 */
	public static void agregarCerditoVietnamita(CerditoVietnamita cv, ArrayList<CerditoVietnamita> listaDeCerditos) {
		listaDeCerditos.add(cv);
	}
	
	/**
	 * Este m�todo permite buscar un cerdito vietnamita en una lista dado el nombre del cerdito
	 * @param nombre el nombre que se va a buscar
	 * @param listaDeCerditos una lista de cerditos
	 * @return 
	 */
	public static String buscarPorAtributoNombre(String nombre, ArrayList<CerditoVietnamita> listaDeCerditos) {
		for(CerditoVietnamita cv : listaDeCerditos) {
			if (cv.getNombre().equals(nombre)) {
				return nombre;
			}
		}
		
		return "No se encontr� el cerdito";
	}
	
	/**
	 * Este m�todo permite mostrar todos los cerditos en una lista de cerditos.
	 * 
	 * @param listaDeCerditos la lista de cerditos que se va a recorrer para
	 * que se impriman los cerditos.
	 */
	public static void buscarCerditos(ArrayList<CerditoVietnamita> listaDeCerditos) {
		System.out.println();
		listaDeCerditos.forEach(cerdito -> {
			System.out.println(cerdito.getNombre() + " es un cerdito vietnamita");
		});
	}
}
