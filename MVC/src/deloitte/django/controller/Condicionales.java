package deloitte.django.controller;

public class Condicionales {

	/**
	 * This method executes a complex calculation given by a mexican ex-president.
	 * 
	 * @return a Boolean type value, true if Oaxaca is near, false if that's not the
	 *         case
	 */
	public static Boolean isOaxacaNear() {
		return (5 < 1) ? true : false;
	}

	/**
	 * This method allows user to know if a given name is long or not.
	 * 
	 * @param myName write your name to know if it is a long name
	 * @return a Boolean type value, true if your name is a long name, false if it
	 *         is not.
	 */
	public static Boolean isMyNameALongName(String myName) {
		return (myName.length() > 9) ? true : false;
	}

	/**
	 * This method allows user to know if a number is even or not.
	 * 
	 * @param myInteger the integer the user is curious about
	 * @return a Boolean type value, true if it is an even number or false it that's
	 *         not the case.
	 */
	public static Boolean isMyIntEven(int myInteger) {
		return ((myInteger % 2) == 0) ? true : false;
	}

	/**
	 * This method allows user to know if a given number can pass a weird condition
	 * or cannot.
	 * 
	 * @param value is the number given by the user to be evaluated
	 * @return a Boolean type value, true if it the condition is true, false if not.
	 */
	public static Boolean evaluateAWeirdCondition(int value) {
		if (((value / 2) == 0) && ((value * 0.77) > 0.4332)) {
			return true;
		}

		return false;
	}

	/**
	 * This method allows the user to know if a given string is Hi or Bye.
	 * 
	 * @param myWord is the word that is gonna be evaluated.
	 */
	public static void didIWriteHiOrBye(String myWord) {
		if (myWord.equals("Hi")) {
			System.out.println("You wrote Hi!");
		} else if (myWord.equals("Bye")) {
			System.out.println("You wrote Bye!");
		} else {
			System.out.println("You didn't write Hi or Bye!");
		}
	}
}
