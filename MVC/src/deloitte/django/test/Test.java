package deloitte.django.test;

import java.util.ArrayList;

import deloitte.django.controller.ControllerCerditoVietnamita;
import deloitte.django.model.CerditoVietnamita;

/**
 * Clase de pruebas en donde se verifica que todo funcione correctamente
 * 
 * @author rubemendoza
 *
 */
public class Test {

	public static void main(String[] args) {
		ArrayList<CerditoVietnamita> listaDeCerditos = new ArrayList<CerditoVietnamita>();
		
		ControllerCerditoVietnamita.agregarCerditoVietnamita(12, 1233, true, false, "Abdul",
				"Suave", "Q", "Q", "Q",
				"Q", "Q", "Q", "Q", "Q",
				"Q", "Q", "Q", "Q", "Q", "Q",
				listaDeCerditos);
		
		CerditoVietnamita cerdito = new CerditoVietnamita(12, 2678, true, false, "Rajesh",
				"Suave", "A", "Q", "Q",
				"Q", "Q", "A", "Q", "Q",
				"Q", "Q", "R", "U", "O", "Q");
		
		ControllerCerditoVietnamita.agregarCerditoVietnamita(cerdito, listaDeCerditos);
		ControllerCerditoVietnamita.buscarPorAtributoNombre("Abdul", listaDeCerditos);
		ControllerCerditoVietnamita.buscarCerditos(listaDeCerditos);
	}

}
